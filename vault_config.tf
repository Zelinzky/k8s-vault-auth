locals {
  normalized_cluster_name = lower(replace(var.k8s_cluster_name, "/ +/", "-"))
}
###
# Auth backend configuration

resource "vault_auth_backend" "kubernetes_auth_vault" {
  type = "kubernetes"
  path = "/kubernetes-${local.normalized_cluster_name}"
}

resource "vault_kubernetes_auth_backend_config" "kubernetes_auth_config" {
  backend = vault_auth_backend.kubernetes_auth_vault.path
  kubernetes_host = var.k8s_cluster_endpoint
  kubernetes_ca_cert = base64decode(data.kubernetes_secret.vault_auth_service_account_deffault_secret.data["ca.crt"])
  token_reviewer_jwt = base64decode(data.kubernetes_secret.vault_auth_service_account_deffault_secret.data["token"])
}

###
# Role creation for service accounts with secret access

resource "vault_kubernetes_auth_backend_role" "kubernetes_auth_role" {
  count = length(var.service_accounts_with_vault_access)
  backend = vault_auth_backend.kubernetes_auth_vault.path
  bound_service_account_names = [var.service_accounts_with_vault_access[count.index].service_account]
  bound_service_account_namespaces = [var.service_accounts_with_vault_access[count.index].namespace]
  role_name = "kubernetes-${local.normalized_cluster_name}-${var.service_accounts_with_vault_access[count.index].service_account}-role"
  token_policies = var.service_accounts_with_vault_access[count.index].vault_policies
  token_ttl = var.service_accounts_with_vault_access[count.index].vault_token_ttl
}