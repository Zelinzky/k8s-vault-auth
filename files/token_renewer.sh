#!/usr/bin/env bash

###
# Install kubectl
curl -LO https://storage.googleapis.com/kubernetes-release/release/"$(curl -s https://storage.googleapis.com/kubernetes-release/release/stable.txt)"/bin/linux/amd64/kubectl
chmod +x ./kubectl
mv ./kubectl /usr/local/bin/kubectl

###
# Install vault CLI
curl -sSLo /tmp/vault.zip https://releases.hashicorp.com/vault/"${VAULT_VERSION}"/vault_"${VAULT_VERSION}"_linux_amd64.zip
unzip -d /usr/local/bin /tmp/vault.zip
chmod +x /usr/local/bin/vault

###
# Get token and create secret
SA_TOKEN=$(cat /var/run/secrets/kubernetes.io/serviceaccount/token)
LOGIN_TOKEN=$(vault write auth/kubernetes/login role="${VAULT_ROLE}" jwt="${SA_TOKEN}" | grep "token " | awk '{print $2}')
kubectl create secret generic "${SECRET_NAME}" --from-literal=token="${LOGIN_TOKEN}" --dry-run -o yaml | kubectl apply -f -
