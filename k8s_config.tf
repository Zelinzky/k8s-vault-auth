locals {
  service_account_with_k8s_secret_access = [for sa in var.service_accounts_with_vault_access : sa if sa.create_k8s_secret == true]
  vault_auth_service_account = "vault-auth"
  vault_auth_service_account_namespace = "default"
}

###
# Cluster auth service account

resource "kubernetes_service_account" "vault_auth_service_account" {
  metadata {
    name = local.vault_auth_service_account
    namespace = local.vault_auth_service_account_namespace
  }
}

resource "kubernetes_role_binding" "vault_service_account_role_binding" {
  metadata {
    name = "role-tokenreview-binding"
    namespace = local.vault_auth_service_account_namespace
  }
  role_ref {
    api_group = "rbac.authorization.k8s.io"
    kind = "ClusterRole"
    name = "system:auth-delegator"
  }
  subject {
    kind = "ServiceAccount"
    name = local.vault_auth_service_account
    namespace = local.vault_auth_service_account_namespace
  }
  depends_on = [kubernetes_service_account.vault_auth_service_account]
}

data "kubernetes_secret" "vault_auth_service_account_deffault_secret" {
  metadata {
    name = kubernetes_service_account.vault_auth_service_account.default_secret_name
  }
}

###
# Secret access service accounts
resource "kubernetes_service_account" "secret_access_service_account" {
  count = length(var.service_accounts_with_vault_access)
  metadata {
    name = var.service_accounts_with_vault_access[count.index].service_account
    namespace = var.service_accounts_with_vault_access[count.index].namespace
  }
}

resource "kubernetes_role" "vault_service_account_role_secret" {
  count = length(local.service_account_with_k8s_secret_access)
  metadata {
    name = "role-${local.service_account_with_k8s_secret_access[count.index].service_account}"
    namespace = local.service_account_with_k8s_secret_access[count.index].namespace
  }
  rule {
    api_groups = [""]
    resources = ["secrets"]
    verbs = ["*"]
  }
}

resource "kubernetes_role_binding" "vault_service_account_role_secret_binding" {
  count = length(local.service_account_with_k8s_secret_access)
  metadata {
    name = "vault-secretadmin-rolebinding-${local.service_account_with_k8s_secret_access[count.index].service_account}"
    namespace = local.service_account_with_k8s_secret_access[count.index].namespace
  }
  role_ref {
    api_group = "rbac.authorization.k8s.io"
    kind = "Role"
    name = "role-${local.service_account_with_k8s_secret_access[count.index].service_account}"
  }
  subject {
    kind = "ServiceAccount"
    name = local.service_account_with_k8s_secret_access[count.index].service_account
    namespace = local.service_account_with_k8s_secret_access[count.index].namespace
  }
}

###
# Cronjob

data "local_file" "token_renewer" {
  filename = "${path.module}/files/token_renewer.sh"
}

resource "kubernetes_cron_job" "token_renewer" {
  count = length(local.service_account_with_k8s_secret_access)
  metadata {
    name = "vault-token-renewer-${local.service_account_with_k8s_secret_access[count.index].service_account}"
    namespace = local.service_account_with_k8s_secret_access[count.index].namespace
  }
  spec {
    concurrency_policy = "Replace"
    schedule = local.service_account_with_k8s_secret_access[count.index].schedule
    job_template {
      metadata {}
      spec {
        template {
          metadata {}
          spec {
            service_account_name = local.service_account_with_k8s_secret_access[count.index].service_account
            container {
              name = "vault_token_renewer"
              image = "alpine:latest"
              image_pull_policy = "Always"
              command = ["/bin/sh"]
              args = [data.local_file.token_renewer.content]
              env {
                name = "VAULT_ADDR"
                value = var.vault_addr
              }
              env {
                name = "VAULT_VERSION"
                value = var.vault_version
              }
              env {
                name = "VAULT_ROLE"
                value = "kubernetes-${local.normalized_cluster_name}-${local.service_account_with_k8s_secret_access[count.index].service_account}-role"
              }
              env {
                name = "SECRET_NAME"
                value = local.service_account_with_k8s_secret_access[count.index].secret_name
              }
            }
          }
        }
      }
    }
  }
}

