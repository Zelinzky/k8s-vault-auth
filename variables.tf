variable "k8s_cluster_name" {
  description = "Name of the cluster it's being connected with vault"
  type = string
}

variable "k8s_cluster_endpoint" {
  description = "Endpoint where we can reach the cluster api"
  type = string
}

variable "service_accounts_with_vault_access" {
  description = "List of service accounts with their respective namespaces, vault policies access & ttl(seconds)"
  type = list(object({
    service_account = string
    namespace = string
    vault_policies = list(string)
    vault_token_ttl = number
    create_k8s_secret = bool # Determines if said SA can access the k8s secrets and if that SA will write its token into it
    secret_name = string # k8s secret name where th sv account will save its token
    schedule = string # Cronjob schedule for token renewal, make sure to use a schedule that matches the ttl of the token
  }))
}

variable "vault_addr" {
  description = "Vault address"
  type = string
  default = ""
}

variable "vault_version" {
  description = "vault cli version to use on the token renewer"
  type = string
  default = "1.4.1"
}