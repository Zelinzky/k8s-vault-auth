provider "kubernetes" {
  version = "1.11.1"
}

provider "vault" {
  version = "2.9.0"
}